package edu.ifpb.pp.exemplos;

/**
 *
 * @author job
 */
public class Resposta {

    private String nome;
    private String cpf;
    private int ids;

    public Resposta() {
    }

//    public Resposta(String nome, String cpf) {
//        this.nome = nome;
//        this.cpf = cpf;
//    }
    public Resposta(String nome, String cpf, int ids) {
        this.nome = nome;
        this.cpf = cpf;
        this.ids = ids;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getId() {
        return ids;
    }

    public void setId(int id) {
        this.ids = id;
    }

}
