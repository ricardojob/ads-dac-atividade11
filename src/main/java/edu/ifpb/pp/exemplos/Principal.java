/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pp.exemplos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author job
 */
public class Principal {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("edu.ifpb.pp_Exemplos_jar_1.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        
        Query query = em.createNativeQuery("SELECT nome, cpf FROM TESTE.PESSOA");
        for (Object valor : query.getResultList()) {
            Object[] a = (Object[]) valor;
            System.out.println(a[0]);
            System.out.println(a[1]);
        }
        
        Query query2 = em.createNativeQuery("SELECT nome, cpf, id FROM TESTE.PESSOA", "resposta.pessoa");
        for (Object valor : query2.getResultList()) { 
            Resposta resposta = (Resposta) valor;
            System.out.println(resposta.getNome() + " "+ resposta.getCpf());
        }
        
         
        TypedQuery<Resposta> query3 = em.createNamedQuery("teste.pessoa", Resposta.class);
        for (Resposta resposta : query3.getResultList()) {
            System.out.println(resposta.getNome() + " " + resposta.getCpf());
        }

    }

}
