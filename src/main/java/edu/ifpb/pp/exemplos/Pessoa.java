package edu.ifpb.pp.exemplos;

import java.io.Serializable;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author job
 */
@SqlResultSetMapping(
        name = "resposta.pessoa",
        classes = {
            @ConstructorResult(targetClass = Resposta.class,
                    columns = {
                        @ColumnResult(name = "nome", type = String.class),
                        @ColumnResult(name = "cpf", type = String.class),
                        @ColumnResult(name = "id", type = int.class)})
        })
@NamedNativeQuery(
        name = "teste.pessoa",
        query = "SELECT nome, cpf, id FROM TESTE.PESSOA",
        resultSetMapping = "resposta.pessoa"
)
@Entity
public class Pessoa implements Serializable {

    @Id
    private int id;
    private String nome;
    private String cpf;

    public Pessoa() {
    }

    public Pessoa(int id, String nome, String cpf) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
